Download ketiga file, tambahkan file film durasi 5 jam dan rename jadi Black.Mirror.Bandersnatch.2018.mkv, buka html di web browser. Selamat menikmati

![alt text](https://camo.githubusercontent.com/08915b901ca8408362d958788cb429d959079dde/68747470733a2f2f692e696d6775722e636f6d2f767664785576792e6a7067)

* F - Fullscreen
* R - Restart video
* → - Jump to the next segment (or to the next interaction zone)
* ← - Jump to the previous segment
* Space - Play/Pause

Credit to joric, jonluka & jolbol1 (Github)

Program bukan buatan saya.